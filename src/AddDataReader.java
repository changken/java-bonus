import java.io.*;
import java.util.*;

public class AddDataReader implements DataProvider {
	
	protected int[] r_i;
	protected int[][] r_ij;
	protected int[] d_i;
	protected int[][] d_ij;
	protected int n;
	protected int m;
	
	public AddDataReader(File file) {
		try {
			Scanner reader = new Scanner(file);
			
			String n_m = reader.nextLine();
			n = Integer.parseInt(n_m.substring(2, 4));
			m = Integer.parseInt(n_m.substring(8));
			
			//initial array
			r_i = new int[n];
			r_ij = new int[n][n];
			d_i = new int[n];
			d_ij = new int[n][n];
			
			reader.nextLine();//r_i (for i=1 to n)=
			String[] r_i_d = reader.nextLine().trim().split("\\s+");
			for(int i=0; i<r_i_d.length; i++) {
				r_i[i] = Integer.parseInt(r_i_d[i]);
			}
			reader.nextLine();// \n
			reader.nextLine();//r_ij (for i,j=1 to n)=
			for(int i=0; i<n; i++) {
				String[] r_ij_d = reader.nextLine().trim().split("\\s+");
				for(int j=0; j<r_ij_d.length; j++) {
					r_ij[i][j] = Integer.parseInt(r_ij_d[j]);
				}
			}
			reader.nextLine();//d_i (for i=1 to n)=
			String[] d_i_d = reader.nextLine().trim().split("\\s+");
			for(int i=0; i<d_i_d.length; i++) {
				d_i[i] = Integer.parseInt(d_i_d[i]);
			}
			reader.nextLine();// \n
			reader.nextLine();//d_ij (for i,j=1 to n)=
			for(int i=0; i<n; i++) {
				String[] d_ij_d = reader.nextLine().trim().split("\\s+");
				for(int j=0; j<d_ij_d.length; j++) {
					d_ij[i][j] = Integer.parseInt(d_ij_d[j]);
				}
			}
			
			reader.close();
		}catch(FileNotFoundException e) {
			System.out.println(e);
		}
	}

	@Override
	public int getN() {
		// TODO 自動產生的方法 Stub
		return n;
	}

	@Override
	public int getM() {
		// TODO 自動產生的方法 Stub
		return m;
	}

	@Override
	public int[] getRi() {
		// TODO 自動產生的方法 Stub
		return r_i;
	}

	@Override
	public int[][] getRij() {
		// TODO 自動產生的方法 Stub
		return r_ij;
	}

	@Override
	public int[] getDi() {
		// TODO 自動產生的方法 Stub
		return d_i;
	}

	@Override
	public int[][] getDij() {
		// TODO 自動產生的方法 Stub
		return d_ij;
	}

}
