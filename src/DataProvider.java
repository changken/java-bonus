
public interface DataProvider {
	public int getN();
	public int getM();
	public int[] getRi();
	public int[][] getRij();
	public int[] getDi();
	public int[][] getDij();
}
