import java.io.*;

public class TestAddDataReader {

	public static void main(String[] args) {
		DataProvider reader = new AddDataReader(new File("Coefficients.txt"));
		System.out.println(reader.getN());
		System.out.println(reader.getM());
		
		System.out.println("R_i");
		for(int e : reader.getRi()) {
			System.out.printf("%d\t", e);
		}
		
		System.out.println("\nR_ij");
		for(int[] eArr : reader.getRij()) {
			for(int e : eArr) {
				System.out.printf("%d\t", e);
			}
			System.out.println();
		}
		
		System.out.println("\nD_i");
		for(int e : reader.getDi()) {
			System.out.printf("%d\t", e);
		}
		
		System.out.println("\nD_ij");
		for(int[] eArr : reader.getDij()) {
			for(int e : eArr) {
				System.out.printf("%d\t", e);
			}
			System.out.println();
		}
		
	}

}
