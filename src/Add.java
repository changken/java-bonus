import java.io.File;

public class Add {
	
	protected int[][] x;
	protected int[] obj;
	protected int Point_obj;
	protected int record;
	protected int interationAmount;
	
	//from other class
	protected int[] r_i;
	protected int[][] r_ij;
	protected int[] d_i;
	protected int[][] d_ij;
	protected int n;
	protected int m;
	
	public Add(DataProvider provider, int record, int interationAmount)
	{
		//initial problem data
		n = provider.getN();
		m = provider.getM();
		r_i = provider.getRi();
		r_ij = provider.getRij();
		d_i = provider.getDi();
		d_ij = provider.getDij();
		
		//initial solution array and loop times
		this.record = record;
		this.interationAmount = interationAmount;
		x = new int[record][n];
		obj = new int[record];
	}
	
	public Add(DataProvider provider)
	{
		//initial problem data
		n = provider.getN();
		m = provider.getM();
		r_i = provider.getRi();
		r_ij = provider.getRij();
		d_i = provider.getDi();
		d_ij = provider.getDij();
		
		//initial solution array and loop times
		record = 200;
		interationAmount = 10000;
		x = new int[record][n];
		obj = new int[record];
	}
	
	public int[] getBestX() {
		return x[Point_obj];
	}
	
	public int getBestObj() {
		return obj[Point_obj];
	}
	
	public int object(int index) {
		int totRiXi = 0;
		for(int i=0; i<n; i++) {
			totRiXi += r_i[i] * x[index][i];
		}
		int totRijXiXj = 0;
		for(int i=0; i<n; i++) {
			for(int j=i+1; j<n; j++) {
				totRijXiXj += r_ij[i][j] * x[index][i] * x[index][j];
			}
		}
		return totRiXi + totRijXiXj;
	}
	
	public int subject(int index) {
		int totDiXi = 0;
		for(int i=0; i<n; i++) {
			totDiXi += d_i[i] * x[index][i];
		}
		int totDijXiXj = 0;
		for(int i=0; i<n; i++) {
			for(int j=i+1; j<n; j++) {
				totDijXiXj += d_ij[i][j] * x[index][i] *x[index][j];
			}
		}
		return totDiXi - totDijXiXj;
	}
	
	public int sumOfX(int index)
	{
		int tot = 0;
		
		for(int i=0; i<n; i++) {
			tot += x[index][i];
		}
		
		return tot;
	}
	
	public void generateX(int index)
	{
		for(int i=0; i<n; i++) {
			x[index][i] = (int)(Math.random() * 2);
		}
	}
	
	public void run()
	{
		for(int i=0; i<record; i++) {
			do {
				generateX(i);
			}while(sumOfX(i) != 10 || subject(i) > 1328);
			obj[i] = object(i);
		}
		
		Point_obj = 0;
		for(int i=1; i<record; i++) {
			if(obj[Point_obj] < obj[i]) {
				Point_obj = i;
			}
		}
		
		//StringBuffer result = new StringBuffer();
		//result.append(String.format("最佳解: %d\n", obj[Point_obj]));
		System.out.printf("最佳解: %d\n", obj[Point_obj]);
		for(int i=0; i<n; i++) {
			if((i+1)%10!=0) {
				//result.append(String.format("%d\t", x[Point_obj][i]));
				System.out.printf("%d\t", x[Point_obj][i]);
			}else {
				//result.append(String.format("%d\n", x[Point_obj][i]));
				System.out.printf("%d\n", x[Point_obj][i]);
			}
		}
	}
	
	public void amounts()
	{
		for(int interation=0; interation<interationAmount; interation++) {
			for(int i=0; i<record; i++) {
				if(Point_obj != i) {
					do {
						generateX(i);
					}while(sumOfX(i) != 10 || subject(i) > 1328);
					obj[i] = object(i);
				}
			}
			
			for(int i=0; i<record; i++) {
				if(obj[Point_obj] < obj[i]) {
					Point_obj = i;
				}
			}
			
			//StringBuffer result = new StringBuffer();
			//result.append(String.format("\n#%d次最佳解: %d\n", interation+1, obj[Point_obj]));
			System.out.printf("\n#%d次最佳解: %d\n", interation+1, obj[Point_obj]);
			for(int i=0; i<n; i++) {
				if((i+1)%10!=0) {
					//result.append(String.format("%d\t", x[Point_obj][i]));
					System.out.printf("%d\t", x[Point_obj][i]);
				}else {
					//result.append(String.format("%d\n", x[Point_obj][i]));
					System.out.printf("%d\n", x[Point_obj][i]);
				}
			}
		}
	}
	
	public void threadRun() {
		run();
		amounts();
	}
	
	public static void main(String[] args) {
		Add a = new Add(new AddDataReader(new File("Coefficients.txt")), 200, 200000);
		a.run();
		a.amounts();
	}
	

}
