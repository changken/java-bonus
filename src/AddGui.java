import java.awt.EventQueue;

import javax.swing.JFrame;

import java.util.*;
import java.awt.TextArea;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;

public class AddGui {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddGui window = new AddGui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AddGui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 939, 638);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		TextArea result = new TextArea();
		result.setFont(new Font("Dialog", Font.PLAIN, 20));
		result.setBounds(32, 143, 842, 406);
		frame.getContentPane().add(result);
		
		JButton exec = new JButton("Run");
		exec.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Add add = new Add(new AddDataReader(new File("Coefficients.txt")), 200, 10000);
				Timer timer = new Timer();
				
				new Thread(()-> {
					add.threadRun();
				}).start();
				
				timer.schedule(new TimerTask() {
					public void run() {
						result.setText(String.format("%s�̨έ�:%d\n", result.getText(), add.getBestObj()));
					}
				}, 0, 1000);
			}
		});
		exec.setFont(new Font("�s�ө���", Font.PLAIN, 20));
		exec.setBounds(42, 48, 156, 48);
		frame.getContentPane().add(exec);
	}
}
